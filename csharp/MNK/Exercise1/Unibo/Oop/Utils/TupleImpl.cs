﻿using System;
using System.Linq;

namespace Unibo.Oop.Utils
{
    internal class TupleImpl : ITuple
    {
        private object[] Items { get; }

        public TupleImpl(object[] args)
        {
            if (args != null)
            {
                Items = args;
            }
            else
            {
                throw new ArgumentNullException();
            }
        }

        public object this[int i] => this.Items[i]; 

        public int Length => this.Items.Length;

        public object[] ToArray() => this.Items;

        public override bool Equals(object obj) => obj != null && obj is TupleImpl 
            && Enumerable.SequenceEqual(this.ToArray(), ((TupleImpl)obj).ToArray());

        public override int GetHashCode() => this.Items.Select(o => o.GetHashCode()).Count();

        public override string ToString() => 
            "(" + this.Items.Select(o => o.ToString()).Aggregate((s1,s2) => s1 + ", " + s2) + ")";      
    }
}
